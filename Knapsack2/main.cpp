#include <iostream>
#include <vector>
#include <tuple>
#include <fstream>
#include <sstream>
#include <stdexcept>



using weight = std::tuple<int, int>;
using problem = std::vector<weight>;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                  Declarations                                             */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void loadObjects(problem &, int &, std::string);
void solveProblem(  problem                    inProblem, 
                    int                        inMaxWeight,
                    std::string              & inoutBestPath,
                    int                      & inoutBestWeight,
                    int                      & inoutBestValue,
                    int                        inWeight,
                    int                        inValue,
                    std::string                inUsedObjects,
                    int                        inDepth);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                               Main                                        */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int main(int argc, char** argv)
{
    (void)argc;
    (void)argv;

    std::string file_name = "./test_files/ex0.txt";

    if (argc > 1)
    {
        file_name = argv[1];
    } 

    problem         sac;
    int             retCode = 0;
    int             maxWeight;
    std::string     bestPath = "";
    int             bestWeight = 0;
    int             bestValue = 0;


    try{
        loadObjects(sac, maxWeight, file_name);

        solveProblem(sac, maxWeight, bestPath, bestWeight, bestValue, 0, 0, "", 0);

        std::cout << "Solution : " << bestPath
              << " poids : "      << bestWeight
              << " valeur : "     << bestValue
              << std::endl;

    }
    catch(std::invalid_argument & e)
    {
        std::cout << e.what() << std::endl;
        retCode = -1;
    }

    
              
    

    return retCode;
}



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                               loadObjet                                   */
/*                                                                           */
/*   ouvre un fichier de parametre et remplit les parametres du probleme     */
/*                                                                           */
/*   Entree:                                                                 */
/*     - inFileName : nom du fichier a ouvrir                                */
/*                                                                           */
/*   Entree-sortie:                                                          */
/*     - unoutProblem : vecteur contenant les poids                          */
/*     - inoutMaxWeight : le poids maximal du sac à dos                      */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void loadObjects(problem & inoutProblem,
                 int & inoutMaxWeight, 
                 std::string inFileName)
{
    int tempPoids;
    int tempValeur;

    std::ifstream file;
    
    file.open(inFileName);

    if(file)
    {
        file >> inoutMaxWeight;

        while(file)
        {
            file >> tempPoids >> tempValeur;
            inoutProblem.push_back(weight{tempPoids, tempValeur});
        }
    }
    else
    {
        throw std::invalid_argument("Fichier non ouvert");
    }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                               solveProblem                                */
/*                                                                           */
/*  resout le probleme avec les tableaux en parametres et donne la meilleure */
/*  combinaison                                                              */
/*                                                                           */
/*   Entree:                                                                 */
/*     - inProblem : tableau contenant les poids à mettre dans le sac        */
/*     - inmaxWeight : capacite maximale du sac                              */
/*     - inWeight : le poids du noeud sur le tour actuel                     */
/*     - inValue : la valeur du noeud sur le tour actuel                     */
/*     - inUsedObjects : chaine de caractère traçant l'historique des objets */
/*                       utilisée                                            */
/*     - inDepth : profondeur (progression dans le tableau problème)         */
/*                                                                           */
/*   Entree-sortie:                                                          */
/*     - unoutProblem : vecteur contenant les poids                          */
/*     - inoutMaxWeight : le poids maximal du sac à dos                      */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void solveProblem(  problem                    inProblem, 
                    int                        inMaxWeight,
                    std::string              & inoutBestPath,
                    int                      & inoutBestWeight,
                    int                      & inoutBestValue,
                    int                        inWeight,
                    int                        inValue,
                    std::string                inUsedObjects,
                    int                        inDepth)

{
    if(inWeight <= inMaxWeight) // Test si Branche/Feuille valide
    {
        // Cas où on n'a pas encore parcouru tout le problème
        if(inDepth < inProblem.size()) 
        {
            // Execution branche droite : on utilise l'objet suivant

            // Preparation des valeurs du noeud suivant
            int nextWeight = std::get<0>(inProblem[inDepth]);
            int nextValue  = std::get<1>(inProblem[inDepth]);
            std::string rUsedObjects = 
                inUsedObjects + "{w: " + std::to_string(nextWeight) 
                              + ", v:" + std::to_string(nextValue) 
                              + "} ";

            solveProblem(inProblem, inMaxWeight, inoutBestPath, inoutBestWeight, 
                         inoutBestValue, inWeight+ nextWeight, 
                         inValue + nextValue, rUsedObjects, inDepth +1);
            
            // Execution branche gauche : on n'utilise pas l'objet suivant
            solveProblem(inProblem, inMaxWeight, inoutBestPath, inoutBestWeight, 
                         inoutBestValue, inWeight, inValue, inUsedObjects, 
                         inDepth +1);                                        

        }
        else // Feuille donc enregistrement des données
        {
            if(inValue > inoutBestValue)
            {
                inoutBestValue = inValue;
                inoutBestPath = inUsedObjects;
                inoutBestWeight = inWeight;
            }
        }
    }
}