#ifndef ACC_OBJECT
#define ACC_OBJECT

class Object{
    private:
        int _weight;
        int _value;

    public:
        Object();
        Object(int, int);

        int getWeight() const;
        int getValue() const;

        void setWeight(int);
        void setValue(int);
};

bool operator<(const Object &, const Object &);


#endif