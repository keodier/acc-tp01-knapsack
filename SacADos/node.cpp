#include "node.hpp"

// Attributs statiques
std::vector<Object> Node::_objectList = std::vector<Object>();
int Node::_maxWeight = 0;
int Node::_totalObject = 0;
Node Node::root;



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                             */
/*                        Constructeurs                                        */
/*                                                                             */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
Node::Node(int value, int weight): 
    _value(value), _weight(weight), _lvalue(NULL), _rvalue(NULL) 
    {}

Node::Node():
    _value(0), _weight(0), _lvalue(NULL), _rvalue(NULL) 
    {}




/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                             */
/*                        Methodes de classe                                   */
/*                                                                             */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void Node::setProblem(const std::vector<Object> & inObjectVector, int inMaxWeight)
{
    _objectList = std::move(inObjectVector);
    _maxWeight = inMaxWeight;
    _totalObject = _objectList.size();
}



void Node::generateTree()
{
    root.generateNode(false, -1, 0, 0);
}

void Node::displayTree()
{
    std::cout << "Arbre de resultats" << std::endl;
    root.displayNode(0);
}

void Node::displayLeafs()
{
    root.affLeaf("");
}

void Node::displayLeafsresults()
{
    std::vector<std::string> roads = std::vector<std::string>();
    std::vector<int> weights = std::vector<int>();
    std::vector<int> values = std::vector<int>();
    root.affLeafResult(roads, weights, values, "");

    int i = findMax(values);
    std::cout << roads[i] << " " << weights[i] << " " << values[i] << std::endl;
}



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                             */
/*                        Methodes d'insance                                   */
/*                                                                             */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

void Node::displayNode(int level)
{
    if(_lvalue)
    {
        std::cout << std::endl;
        _lvalue->displayNode(level +1);
    }

    for(int i = 0 ; i < level ;i++)
    {
        std::cout << "        ";
    }
    std::cout << "[ " << _weight << " - " << _value << " ]" << std::endl;

    if(_rvalue)
    {
        _rvalue->displayNode(level + 1);
    }
     
}

void Node::generateNode(bool inIsObjectUsed, int inTreeLevel, int inBaseValue, int inBaseWeight)
{
    _value = inBaseValue;
    _weight = inBaseWeight;

    if(inIsObjectUsed)
    {
        _value  += _objectList[inTreeLevel].getValue();
        _weight += _objectList[inTreeLevel].getWeight();
    }

    if(inTreeLevel < _totalObject-1)
    {
        // Test si poids pas trop grand avant de generer le prochain noeud droite
        // Car utilisation d'un autre objet

        if (_weight  + _objectList[inTreeLevel+1].getWeight () < _maxWeight )
        {
            _rvalue = new Node();
            _rvalue->generateNode(true, inTreeLevel + 1, _value, _weight);
        }

        // Noeud gauche créé dans tous les cas car aucune augmentation de poids
        _lvalue = new Node();
        _lvalue->generateNode(false, inTreeLevel + 1, _value, _weight);
    }
}



Node::~Node()
{
    if(_lvalue)
        delete(_lvalue);

    if(_rvalue)
        delete(_rvalue);
}

void Node::affLeaf(std::string road)
{
    if(_lvalue)
    {
        _lvalue->affLeaf(road + "0");

        if(_rvalue)
        {
            _rvalue->affLeaf(road + "1");
        }
    }
    else
    {
        if(_rvalue)
        {
            _rvalue->affLeaf(road + "1");
        }
        else
        {
            std::cout << "road : " << road  << " | ";
            std::cout << "weight : " << _weight << " | ";
            std::cout << "value : " << _value << " |" << std::endl;
        }
    }
    
    
}


void Node::affLeafResult(std::vector<std::string> & roads , std::vector<int> & weights, std::vector<int> & values, std::string road)
{
    if(_lvalue)
    {
        _lvalue->affLeafResult(roads, weights, values, road + "0");

        if(_rvalue)
        {
            _rvalue->affLeafResult(roads, weights, values,road + "1");
        }
    }
    else
    {
        if(_rvalue)
        {
            _rvalue->affLeafResult(roads, weights, values,road + "1");
        }
        else
        {
            roads.push_back(road);
            weights.push_back(_weight);
            values.push_back(_value);
        }
    }
}

int findMax(std::vector<int> vect)
{
    int maxPos = 0;
    int maxValue = vect[0];
    for(int i = 1; i < vect.size(); i++)
    {
        if(vect[i] > maxValue)
        {
            maxPos = i;
            maxValue = vect[i];
        }
    }
    return maxPos;
}