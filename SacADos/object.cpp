#include "object.hpp"


Object::Object(): _weight(0) , _value(0) {}

Object::Object(int inWeight, int inValue):
    _weight(inWeight), _value(inValue) 
    {}


int Object::getWeight() const
{
    return _weight;
}

int Object::getValue() const
{
    return _value;
}

void Object::setWeight(int inWeight)
{
    _weight = inWeight;
}

void Object::setValue(int inValue)
{
    _value = inValue;
}

bool operator<(const Object & inA, const Object & inB)
{
    return (inA.getValue() == inB.getValue())? (inA.getValue() < inB.getValue()):(inA.getWeight() < inB.getWeight());
}