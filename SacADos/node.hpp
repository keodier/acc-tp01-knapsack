#ifndef ACC_NODE
#define ACC_NODE

#include <vector>
#include <tuple>
#include <iostream>
#include <algorithm>

#include "object.hpp"

class Node{

    private:
        static int _maxWeight;
        static int _totalObject;
        static std::vector<Object> _objectList;
        static Node root;

        int _value;
        int _weight;
        Node * _lvalue;
        Node * _rvalue;

        // Methodes privees recusrives (instance)
        void generateNode(bool, int, int, int);
        void displayNode(int);
        void affLeaf(std::string);
        void affLeafResult(std::vector<std::string> &, std::vector<int> &, std::vector<int> &, std::string);

    public:
        
        Node();
        Node(int, int);
        ~Node();
        
        static void generateTree();
        static void setProblem(const std::vector<Object> &, int);
        static void displayTree();
        static void displayLeafs();
        static void displayLeafsresults();
};

int findMax(std::vector<int>);


#endif