#include "node.hpp"
#include "object.hpp"

#include <iostream>
#include <vector>
#include <fstream>

int main(int argc, char ** argv)
{
    std::vector<Object> objets = std::vector<Object>();

    int capacite;
    int tempPoids;
    int tempValeur;

    std::ifstream file("params.txt");

    if(file)
    {
        file >> capacite;

        while(file)
        {
            file >> tempPoids >> tempValeur;
            objets.push_back(Object(tempPoids, tempValeur));
        }
    }
    
    /*
    std::cout << "Capacite : " << capacite << std::endl;
    for(Object objet : objets)
    {
        std::cout << "[poids : " << objet.getWeight() << " ; valeur : " << objet.getValue() << " ]" << std::endl;
    }*/

    /*
    objets.push_back(Object(4,12));
    objets.push_back(Object(2,2));
    objets.push_back(Object(2,1));
    objets.push_back(Object(10,4));
    objets.push_back(Object(1,1));
    */

    Node::setProblem(objets, capacite);
    Node::generateTree();
    Node::displayLeafs();
    Node::displayLeafsresults();

    return 0;
}






















